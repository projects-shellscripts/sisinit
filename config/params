#!/bin/bash

export PATH=/sbin:/bin:/usr/sbin:/usr/bin

# Arquivo de Log
LOG_OUT="./logs/run.out"
LOG_ERR="./logs/run.err"

SSH_KEY_PATH="/home/vagrant/.ssh/authorized_keys"
SSH_KEY_TEST=$(egrep "fabiano@LATITUDE-E5570" ${SSH_KEY_PATH} |cut -d" " -f3 2>> "${LOG_ERR}")

SSH_RSA_PUB="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDItKrZW9YoyTeumjAkn3E\
LXbSUyWIcWR5dGfLqlfX8kSeVVx70NnLCdeQDjbKKZdPqtV0RSk4Fr5XtsVNt0BudpXxrczr\
dilVLqxn63XyXxk8tFc4ORpTT8rbUFqczJ6Z2k8JkJX1uLuF0UI6kHMF5FvTvweD8jDw761n\
ZfUV2DHf2ipmgTJPSb/kn2B62rXJZ3uEXEhWY/Q0+XLwmPVMp0QTC23kegriuBI10b8rt4bj\
n6MT6qJ5eaWZqFI/Q+jIdTVghVNS2TdnfJ264tx3gHqcjA1jxRzQYmbq5L9mE0IZLYHSo0yO\
HERDvai4kCarwuzM1EzXThtp0EjifA6Eb fabiano@laptop"

ROOT_LOGIN=$(egrep "^PermitRoot" /etc/ssh/sshd_config |cut -d" " -f2)

# Hora
TIME_ZONE="America/Sao_Paulo"

# Configuração de rede
HOST_NAME=""
IP_LOCAL=""
IP_GW=""
IP_DNS1=""
IP_DNS2=""

# Programa RM
RM=rm
OPT_FV="-fv"

# Programa YUM 
# ${YUM} ${YUM_OPT_1} ${YUM_OPT_FLAG_1}
YUM="yum"
YUM_UPD="update"
YUM_OPT_1="install"
YUM_OPT_FLAG_1="-y"

# Programa SYSTEMCTL
# ${SYSCTL} ${SYSTEMCTL_OPT_1}
SYSTEMCTL="systemctl"
SYSTEMCTL_OPT_1="list-unit-files"
SYSTEMCTL_OPT_2="enable"
SYSTEMCTL_OPT_3="disable"
SYSTEMCTL_OPT_4="start"
SYSTEMCTL_OPT_5="stop"
SYSTEMCTL_OPT_6="restart"

# Programa RPM
# ${RPM} ${RPM_OPT}
RPM="rpm"
RPM_OPT="-aq"
RPM_OPT_2="--import"

# Programa GREP
EGREP="egrep"

# Programa CP
# ${CP} ${CP_OPT_1}
CP="cp"
CP_OPT_1="-rfv"

# Timedate
# ${TIMEDATECTL} ${TIMEDATECTL_OPT_1} ${TIME_ZONE}
TIMEDATECTL="timedatectl"
TIMEDATECTL_OPT_1="set-timezone"
TIMEDATECTL_VERIFY=$(${TIMEDATECTL} |${EGREP} "Time\ zone" |cut -d":" -f2 |cut -d"(" -f1 |xargs)

# Selinux
SELINUX_CONFIG="disabled"
SELINUX_PATH="/etc/selinux/config"
SELINUX_VERIFY=$(cat ${SELINUX_PATH} |egrep "^SELINUX=" |cut -d"=" -f2)

# Arquivos para limpeza de conteúdo
CONFIG_FILES="/etc/issue
/etc/motd"

# Arquivos para remoção
ROOT_FILES="/root/anaconda-ks.cfg
/root/original-ks.cfg
/root/openscap_data"

# NTP
NTP_FILE="/etc/ntp/step-tickers"
NTP_VERIFY=$(${EGREP} "^0.centos" ${NTP_FILE} 1>> ${LOG_OUT} 2>> ${LOG_ERR})

# Repositorios
EPEL_KEY="https://archive.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7"
EPEL_KEY_FILE="/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7"
REPO_DEF="epel-release"

# Programas padrão
PACK_DEF="vim-enhanced
unzip
tree
net-tools
bind-utils
ntpdate
tcpdump
nmap-ncat
nmap
htop
tmux
ansible
"
# Serviços padrão
SERV_DIS="NetworkManager.service
NetworkManager-dispatcher.service
NetworkManager-wait-online.service
firewalld.service"

SERV_ENB="ntpdate.service
network.service
"

# CUSTOM
RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m"
YELLOW="\033[1;33m"